//
//  SOTAlertMessage.m
//  PjCagliari
//
//  Created by Oni_01 on 26/03/13.
//  Copyright (c) 2013 Oni_01. All rights reserved.
//

#import "SOTAlertMessage.h"

@implementation SOTAlertMessage

-(id)init{
    if(self == [super init]){
        //Preparo la vista e le subview
        self.view = [[SOTAlertMessageView alloc] initWithFrame:CGRectMake(0,
                                                                          -50,
                                                                          [UIScreen mainScreen].bounds.size.width,
                                                                          50)];
        [self.view setBackgroundColor:[UIColor whiteColor]];
        //Immagine
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5,
                                                                       40, 40)];
        
        [self.view addSubview: self.imageView];
        
        //Titolo
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,
                                                                    3,
                                                                    CGRectGetWidth(self.view.frame) -55,
                                                                    15)];
        [self.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        [self.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.titleLabel setShadowColor:[UIColor whiteColor]];
        [self.titleLabel setShadowOffset:CGSizeMake(0, 1)];
        [self.view addSubview:self.titleLabel];
        
        //Descrizione
        self.detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,
                                                                     18,
                                                                     CGRectGetWidth(self.view.frame) -55,
                                                                     30)];
        [self.detailLabel setFont:[UIFont systemFontOfSize:10]];
        [self.detailLabel setBackgroundColor:[UIColor clearColor]];
        [self.detailLabel setNumberOfLines:2];
        
        [self.view addSubview:self.detailLabel];
        
        //extra
        [self.view setUserInteractionEnabled:NO];
        
        //imposto lo stato del messaggio
        [self setMessageState:SOTAlertMessageStateCommunication];
    }
    return self;
}


-(id)initWithState:(enum SOTAlertMessageState)state{
    if(self == [self init]){
        [self setMessageState: state];
    }
    return self;
}

-(id)initWithTitle:(NSString *)title Message:(NSString *)msg{
    if(self == [self init]){
        
    }
    return self;
}

-(void)show{
    float finalY = 0;
    id nav = [UIApplication sharedApplication].keyWindow.rootViewController;
    if ([nav isKindOfClass:[UINavigationController class]]) {
        [[(UINavigationController *)nav topViewController].view addSubview:self.view];
    }else{
        [[[UIApplication sharedApplication] keyWindow] addSubview:self.view];
        if (![UIApplication sharedApplication].statusBarHidden){
            finalY = finalY +20;
        }
    }
    [UIView animateWithDuration:0.25
                          delay:0
                        options:(UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionCurveEaseOut)
                     animations:^{
                         [self.view setFrame:CGRectMake(0, finalY,
                                                        CGRectGetWidth(self.view.frame) ,
                                                        CGRectGetHeight(self.view.frame))];
                     }
                     completion:^(BOOL finished) {
        [UIView animateWithDuration:0.25
                              delay:0.5
                            options:(UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionCurveEaseIn)
                         animations:^{
                             [self.view setFrame:CGRectMake(0, -50,
                                                            CGRectGetWidth(self.view.frame) ,
                                                            CGRectGetHeight(self.view.frame))];
                         }
                         completion:^(BOOL finished) {
                             [self.view removeFromSuperview];
                         }];
                     }];
}

-(void)setMessageState:(enum SOTAlertMessageState)messageState{
    self->msgState = messageState;
    
    //Imposto i colori e l'immagine correlata allo stato indicato
    switch (msgState) {
        case SOTAlertMessageStateCommunication:
            [self.view setColor:[UIColor colorWithRed:60/255 green:161/255 blue:255/255 alpha:1]];
            [self.imageView setImage:[UIImage imageNamed:@"SOTAlertViewCommunication.png"]];
            break;
            
        case SOTAlertMessageStateSuccess:
            [self.view setColor:[UIColor colorWithRed:60/255 green:255/255 blue:97/255 alpha:1]];
            [self.imageView setImage:[UIImage imageNamed:@"SOTAlertViewOK.png"]];
            break;
            
        case SOTAlertMessageStateError:
            [self.view setColor:[UIColor colorWithRed:255/255 green:97/255 blue:70/255 alpha:1]];
            [self.imageView setImage:[UIImage imageNamed:@"SOTAlertViewError.png"]];
            break;
    }
    CGFloat definedColorHSBA[4];
    [self.view.color getHue: &definedColorHSBA[0]
                 saturation: &definedColorHSBA[1]
                 brightness: &definedColorHSBA[2]
                      alpha: &definedColorHSBA[3]];
    
    [self.titleLabel setTextColor:[UIColor colorWithHue: definedColorHSBA[0]
                                             saturation: definedColorHSBA[1]
                                             brightness: 0.328
                                                  alpha: 1]];
    
    [self.detailLabel setTextColor:[UIColor colorWithHue: definedColorHSBA[0]
                                              saturation: definedColorHSBA[1]
                                              brightness: 0.328
                                                   alpha: 1]];
    
    [self.view setNeedsDisplay];
}

-(enum SOTAlertMessageState)messageState{
    return self->msgState;
}

@end

@implementation SOTAlertMessageView

-(id)init{
    if(self==[super init]){
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

-(void)drawRect:(CGRect)rect{
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* color = self.color;
    UIColor* definedColor = [color colorWithAlphaComponent: 0.8];
    CGFloat definedColorRGBA[4];
    [definedColor getRed: &definedColorRGBA[0] green: &definedColorRGBA[1] blue: &definedColorRGBA[2] alpha: &definedColorRGBA[3]];
    
    CGFloat definedColorHSBA[4];
    [definedColor getHue: &definedColorHSBA[0] saturation: &definedColorHSBA[1] brightness: &definedColorHSBA[2] alpha: &definedColorHSBA[3]];
    
    UIColor* light = [UIColor colorWithRed: (definedColorRGBA[0] * 0.201 + 0.799) green: (definedColorRGBA[1] * 0.201 + 0.799) blue: (definedColorRGBA[2] * 0.201 + 0.799) alpha: (definedColorRGBA[3] * 0.201 + 0.799)];
    UIColor* dark = [UIColor colorWithHue: definedColorHSBA[0] saturation: definedColorHSBA[1] brightness: 0.328 alpha: definedColorHSBA[3]];
    
    //// Gradient Declarations
    NSArray* gradientColors = [NSArray arrayWithObjects:
                               (id)color.CGColor,
                               (id)light.CGColor, nil];
    CGFloat gradientLocations[] = {0, 1};
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)gradientColors, gradientLocations);
    
    //// Frames
    CGRect frame = rect;
    
    
    //// Rectangle Drawing
    CGRect rectangleRect = CGRectMake(CGRectGetMinX(frame) - 0.5, CGRectGetMinY(frame) - 0.5, CGRectGetWidth(frame) - -1, CGRectGetHeight(frame) - 0);
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: rectangleRect];
    CGContextSaveGState(context);
    [rectanglePath addClip];
    CGContextDrawLinearGradient(context, gradient,
                                CGPointMake(CGRectGetMidX(rectangleRect), CGRectGetMaxY(rectangleRect)),
                                CGPointMake(CGRectGetMidX(rectangleRect), CGRectGetMinY(rectangleRect)),
                                0);
    CGContextRestoreGState(context);
    [dark setStroke];
    rectanglePath.lineWidth = 1;
    [rectanglePath stroke];
    
    
    //// Cleanup
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

@end

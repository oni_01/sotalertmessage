//
//  SOTAlertMessage.h
//  PjCagliari
//
//  Created by Oni_01 on 26/03/13.
//  Copyright (c) 2013 Oni_01. All rights reserved.
//

#import <Foundation/Foundation.h>

enum SOTAlertMessageState {
        SOTAlertMessageStateError,
        SOTAlertMessageStateCommunication,
        SOTAlertMessageStateSuccess
    };

@interface SOTAlertMessageView : UIView

@property (nonatomic, strong) UIColor *color;

@end

@interface SOTAlertMessage : UIView{
    @protected
    enum SOTAlertMessageState msgState;
}

@property (nonatomic) enum SOTAlertMessageState messageState;

@property (nonatomic, strong) SOTAlertMessageView *view;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailLabel;

-(id)initWithState:(enum SOTAlertMessageState)state;

-(id)initWithTitle:(NSString *)title Message:(NSString *)msg;
-(void)show;

@end

